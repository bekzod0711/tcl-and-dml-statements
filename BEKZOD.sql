UPDATE customer
SET first_name = 'YourFirstName', -- Replace with your first name
    last_name = 'YourLastName', -- Replace with your last name
    email = 'YourEmail@example.com' -- Replace with your email
WHERE customer_id IN (
    SELECT customer_id 
    FROM rental
    GROUP BY customer_id
    HAVING COUNT(*) >= 10
) AND customer_id IN (
    SELECT customer_id 
    FROM payment
    GROUP BY customer_id
    HAVING COUNT(*) >= 10
);
UPDATE customer
SET create_date = CURRENT_DATE
WHERE customer_id = YourCustomerID; -- Replace with the actual customer ID
