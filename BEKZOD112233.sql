-- Remove a previously inserted film from the inventory and all corresponding rental records
-- Assuming a CASCADE DELETE setup or manual deletion from related tables

-- First, delete from rental records (if not cascading)
DELETE FROM rental_table
WHERE inventory_id IN (
  SELECT inventory_id FROM inventory_table WHERE film_id = <specific_film_id>
);

-- Then, delete from inventory
DELETE FROM inventory_table WHERE film_id = <specific_film_id>;

-- Finally, delete the film
DELETE FROM film_table WHERE film_id = <specific_film_id>;


-- Remove any records related to a specific customer from all tables except "Customer" and "Inventory"

-- Delete from rental records as an example of related records
-- Repeat similarly for other related records, substituting `rental_table` and relevant columns as necessary
DELETE FROM rental_table WHERE customer_id = <specific_customer_id>;

-- Note: If there are other tables that reference the customer (like payment histories, reviews, etc.),
-- similar DELETE statements would be necessary for each, always ensuring you're not violating foreign key constraints.
-- Make sure to adjust <specific_customer_id> and table/column names based on your actual data schema.

-- IMPORTANT: Replace <specific_film_id> and <specific_customer_id> with the actual IDs you intend to delete.
-- Also, replace table and column names (e.g.,entory_id IN (
 etc.)
-- with the actual names from your database schema.
